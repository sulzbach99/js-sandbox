#!/usr/bin/env node

var readline = require('readline');
var cp = require('child_process');
var finger = cp.spawn('finger', [process.argv[2]]);
var lineReader = readline.createInterface(finger.stdout, finger.stdin);

lineReader.on('line', function(line) {
    console.log(line);
});